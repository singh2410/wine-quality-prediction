# Wine quality prediction using  model
# By- Aarush Kumar

Wine quality prediction model using RandomForestClassifier.
The dataset is related to red variants of the Portuguese “Vinho Verde” wine. These datasets can be viewed as classification or regression tasks. The classes are ordered and not balanced (e.g. there are many more normal wines than excellent or poor ones). 
Steps involved:
1.Importing libraries.
2.Perform exploratory data analysis.
3.Normalization.
4.Implementing classification Model.
5.Check accuracy and predict.
Thankyou!
