#!/usr/bin/env python
# coding: utf-8

# # Wine quality prediction using RandomForestClassifier
# #By- Aarush Kumar
# #Dated: May 21,2021

# In[2]:


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score


# In[3]:


df=pd.read_csv(r'/home/aarush100616/Downloads/Projects/Wine quality/data.csv')
df


# In[5]:


df.shape


# In[6]:


df.head()


# In[7]:


df.isnull().sum()


# In[8]:


df.describe()


# In[13]:


# number of values for each quality
sns.catplot(x='quality', data = df, kind = 'count')


# In[10]:


# volatile acidity vs Quality
plot = plt.figure(figsize=(13,8))
sns.barplot(x='quality', y = 'volatile acidity', data = df)


# In[14]:


# citric acid vs Quality
plot = plt.figure(figsize=(13,8))
sns.barplot(x='quality', y = 'citric acid', data = df)


# In[15]:


#correlation
correlation = df.corr()


# In[16]:


# constructing a heatmap to understand the correlation between the columns
plt.figure(figsize=(13,8))
sns.heatmap(correlation, cbar=True, square=True, fmt = '.1f', annot = True, annot_kws={'size':8}, cmap = 'Blues')


# In[17]:


#data processing
# separate the data and Label
X = df.drop('quality',axis=1)


# In[18]:


print(X)


# In[19]:


#labeling
Y = df['quality'].apply(lambda y_value: 1 if y_value>=7 else 0)


# In[20]:


print(Y)


# In[21]:


#train & test split
X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2, random_state=3)
print(Y.shape, Y_train.shape, Y_test.shape)


# In[22]:


#model training
md = RandomForestClassifier()


# In[23]:


md.fit(X_train, Y_train)


# In[24]:


#model evaluation
#accuracy score
X_test_prediction = md.predict(X_test)
test_data_accuracy = accuracy_score(X_test_prediction, Y_test)


# In[26]:


print('Accuracy : ', test_data_accuracy*100)


# In[33]:


#Predictive System
input_data = (7.5,0.5,0.36,6.1,0.071,17.0,102.0,0.9978,3.35,0.8,10.5)

# changing the input data to a numpy array
input_data_as_numpy_array = np.asarray(input_data)

# reshape the data as we are predicting the label for only one instance
input_data_reshaped = input_data_as_numpy_array.reshape(1,-1)

prediction = md.predict(input_data_reshaped)
print(prediction)

if (prediction[0]==0):
  print('Good Quality Wine')
else:
  print('Bad Quality Wine')

